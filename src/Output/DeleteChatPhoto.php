<?php

namespace dott_xado\TelegramApi\Output;

class DeleteChatPhoto extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}