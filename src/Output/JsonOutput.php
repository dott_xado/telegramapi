<?php

namespace dott_xado\TelegramApi\Output;

class JsonOutput extends Output {

  public function apiRequestJson($parameters) {
    echo json_encode($parameters);
  }
}