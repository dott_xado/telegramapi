<?php

namespace dott_xado\TelegramApi\Output;

class ExportChatInviteLink extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}