<?php

namespace dott_xado\TelegramApi\Output;

class UnbanChatMember extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'user_id',
    ];
  }

  public function setUserId($id) {
    $this->user_id = $id;
  }

}