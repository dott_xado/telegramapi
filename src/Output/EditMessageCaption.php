<?php

namespace dott_xado\TelegramApi\Output;

class EditMessageCaption extends Method {

  protected function getRequiredOptional() {
    return [
      'inline_message_id' => ['chat_id', 'message_id'],
      'chat_id' => ['inline_message_id'],
      'message_id' => ['inline_message_id'],
    ];
  }

}