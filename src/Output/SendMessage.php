<?php

namespace dott_xado\TelegramApi\Output;
use dott_xado\TelegramApi\Entity\InlineKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardRemove;
use dott_xado\TelegramApi\Entity\ForceReply;


class SendMessage extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply']
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'text',
    ];
  }

  public function setText($text) {
    $this->text = $text;
  }

}