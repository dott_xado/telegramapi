<?php

namespace dott_xado\TelegramApi\Output;

use dott_xado\TelegramApi\Entity\Entity;
use dott_xado\TelegramApi\Entity\InlineKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardRemove;
use dott_xado\TelegramApi\Entity\ForceReply;


abstract class Method extends Entity implements MethodInterface {

  public function __construct($data = null) {
    if (!is_null($data)) {
      $subEntities = $this->getSubEntities();
      foreach ($data as $parameter => $value) {
        if (array_key_exists($parameter, $subEntities)) {
          $this->$parameter = $this->createEntities($parameter, $value, $subEntities[$parameter]);
        } else {
          $this->$parameter = $value;
        }
      }
    }
  }

  public function setChatId($id) {
    $this->chat_id = $id;
  }

  public function setParseMode($mode) {
    // HTML or Markdown
    if ($mode === 1) {
      $mode = 'HTML';
    } else {
      $mode = 'Markdown';
    }
    $this->parse_mode = $mode;
  }

  public function disableWebPreview($bool) {
    if ($bool) {
      $this->disable_web_page_preview = TRUE;
    }
  }

  public function disableNotification($bool) {
    if ($bool) {
      $this->disable_notification = TRUE;
    }
  }

  public function setReply($id) {
    if (is_numeric($id)) {
      $this->reply_to_message_id = $id;
    }
  }

  public function setReplyMarkup($markup, $reply_markup = null) {
    if ($markup instanceof InlineKeyboardMarkup
      || $markup instanceof ReplyKeyboardMarkup
      || $markup instanceof ReplyKeyboardRemove
      || $markup instanceof ForceReply) {
      $this->reply_markup = $markup;
    } else if (is_array($markup) && $reply_markup !== null) {
      $className = 'dott_xado\TelegramApi\Entity\\' . $reply_markup;
      $keyboardName = $className::getKeyboardName();
      $this->reply_markup = new $className([$keyboardName => $markup]);
    }

  }
}