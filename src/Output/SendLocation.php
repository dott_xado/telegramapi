<?php

namespace dott_xado\TelegramApi\Output;

class SendLocation extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'latitude',
      'longitude',
    ];
  }

  public function setCoords($lat, $lon) {
    $this->latitude = $lat;
    $this->longitude = $lon;
  }

}