<?php

namespace dott_xado\TelegramApi\Output;

abstract class Output {

  public function response(MethodInterface $response) {
    $response->validate();

    $classNameNamespace = get_class($response);
    preg_match('/[^\\\\]+$/', $classNameNamespace, $matches);
    $method = lcfirst($matches[0]);
    $array = (array) $response;
    $array['method'] = $method;
    return $this->apiRequestJson($array);
  }

  abstract function apiRequestJson($parameters);

}