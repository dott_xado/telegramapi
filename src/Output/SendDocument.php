<?php

namespace dott_xado\TelegramApi\Output;

class SendDocument extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'document',
    ];
  }

  public function setDocument($document) {
    $this->document = $document;
  }

}