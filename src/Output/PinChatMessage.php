<?php

namespace dott_xado\TelegramApi\Output;

class PinChatMessage extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'message_id',
    ];
  }

  public function setMessageId($id) {
    $this->message_id = $id;
  }

}