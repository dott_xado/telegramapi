<?php

namespace dott_xado\TelegramApi\Output;

class SendContact extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'phone_number',
      'first_name',
    ];
  }

  public function setPhone($phone) {
    $this->phone = $phone;
  }

  public function setFirstName($name) {
    $this->first_name = $name;
  }

}