<?php

namespace dott_xado\TelegramApi\Output;

class SendChatAction extends Method {

  protected $actions = [
    'typing',
    'upload_photo',
    'record_video',
    'upload_video',
    'record_audio',
    'upload_audio',
    'upload_document',
    'find_location',
    'record_video_note',
    'upload_video_note',
  ];

  protected function getRequired() {
    return [
      'chat_id',
      'action',
    ];
  }

  public function setAction($action) {
    if (in_array($action, $this->actions)) {
      $this->action = $action;
    }
  }

}