<?php

namespace dott_xado\TelegramApi\Output;

class SendVenue extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'latitude',
      'longitude',
      'title',
      'address',
    ];
  }

  public function setCoords($lat, $lon) {
    $this->latitude = $lat;
    $this->longitude = $lon;
  }

}