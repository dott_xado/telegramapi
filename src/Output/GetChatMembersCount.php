<?php

namespace dott_xado\TelegramApi\Output;

class GetChatMembersCount extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}