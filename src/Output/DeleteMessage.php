<?php

namespace dott_xado\TelegramApi\Output;

class DeleteMessage extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'message_id',
    ];
  }

  public function setMessageId($message_id) {
    $this->message_id = $message_id;
  }

}