<?php

namespace dott_xado\TelegramApi\Output;

class GetUserProfilePhotos extends Method {

  protected function getRequired() {
    return [
      'user_id',
    ];
  }

  public function setUserId($id) {
    $this->user_id = $id;
  }

}