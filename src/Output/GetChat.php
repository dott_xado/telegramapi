<?php

namespace dott_xado\TelegramApi\Output;

class GetChat extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}