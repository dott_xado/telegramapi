<?php

namespace dott_xado\TelegramApi\Output;

class GetFile extends Method {

  protected function getRequired() {
    return [
      'file_id',
    ];
  }

  public function setFileId($id) {
    $this->file_id = $id;
  }

}