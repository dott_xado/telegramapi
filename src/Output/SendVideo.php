<?php

namespace dott_xado\TelegramApi\Output;

class SendVideo extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'video',
    ];
  }

  public function setVideo($video) {
    $this->video = $video;
  }

}