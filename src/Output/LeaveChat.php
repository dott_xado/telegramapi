<?php

namespace dott_xado\TelegramApi\Output;

class LeaveChat extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}