<?php

namespace dott_xado\TelegramApi\Output;

class SetChatPhoto extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'photo',
    ];
  }

  public function setPhoto($photo) {
    $this->photo = $photo;
  }

}