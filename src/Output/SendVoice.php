<?php

namespace dott_xado\TelegramApi\Output;

class SendVoice extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'voice',
    ];
  }

  public function setVoice($voice) {
    $this->voice = $voice;
  }

}