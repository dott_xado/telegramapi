<?php

namespace dott_xado\TelegramApi\Output;

class EditMessageText extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => 'InlineKeyboardMarkup',
    ];
  }

  protected function getRequired() {
    return [
      'text',
    ];
  }

  protected function getRequiredOptional() {
    return [
      'inline_message_id' => ['chat_id', 'message_id'],
      'chat_id' => ['inline_message_id'],
      'message_id' => ['inline_message_id'],
    ];
  }

  public function setText($text) {
    $this->text = $text;
  }

}