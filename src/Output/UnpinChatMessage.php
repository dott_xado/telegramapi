<?php

namespace dott_xado\TelegramApi\Output;

class UnpinChatMessage extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}