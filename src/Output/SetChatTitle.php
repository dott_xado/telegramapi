<?php

namespace dott_xado\TelegramApi\Output;

class SetChatTitle extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'title',
    ];
  }

  public function setUserId($title) {
    $this->title = $title;
  }

}