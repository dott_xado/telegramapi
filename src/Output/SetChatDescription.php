<?php

namespace dott_xado\TelegramApi\Output;

class setChatDescription extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'description',
    ];
  }

  public function setUserId($description) {
    $this->description = $description;
  }

}