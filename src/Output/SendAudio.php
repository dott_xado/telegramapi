<?php

namespace dott_xado\TelegramApi\Output;
use dott_xado\TelegramApi\Entity\InlineKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardMarkup;
use dott_xado\TelegramApi\Entity\ReplyKeyboardRemove;
use dott_xado\TelegramApi\Entity\ForceReply;


class SendAudio extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply']
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'audio',
    ];
  }

  public function setAudio($audio) {
    $this->audio = $audio;
  }

  public function setReply($id) {
    if (is_numeric($id)) {
      $this->reply_to_message_id = $id;
    }
  }

}