<?php

namespace dott_xado\TelegramApi\Output;

class GetChatAdministrators extends Method {

  protected function getRequired() {
    return [
      'chat_id',
    ];
  }

}