<?php

namespace dott_xado\TelegramApi\Output;

class SendVideoNote extends Method {

  protected function getSubEntities() {
    return [
      'reply_markup' => ['InlineKeyboardMarkup', 'ReplyKeyboardMarkup', 'ReplyKeyboardRemove', 'ForceReply'],
    ];
  }

  protected function getRequired() {
    return [
      'chat_id',
      'video_note',
    ];
  }

  public function setVideoNote($video_note) {
    $this->video_note = $video_note;
  }

}