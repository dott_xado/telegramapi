<?php

namespace dott_xado\TelegramApi\Output;

class TelegramOutput extends Output {

  public function apiRequestJson($parameters) {

    if (!$parameters) {
      $parameters = array();
    } else if (!is_array($parameters)) {
      return false;
    }

    //$parameters["method"] = $method;

    $handle = curl_init(API_URL);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);
    curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
    curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

    return $this->send_curl($handle);
  }

  public function send_curl($handle) {

    $response = json_decode(curl_exec($handle), true);

    if ($response === false) {
      curl_close($handle);
      return false;
    }
    
    $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
    curl_close($handle);

    if ($http_code >= 500) {
      // do not wat to DDOS server if something goes wrong
      sleep(100);
    }

    return $response;
  }

}