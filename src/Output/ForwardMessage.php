<?php

namespace dott_xado\TelegramApi\Output;

class ForwardMessage extends Method {

  protected function getRequired() {
    return [
      'chat_id',
      'from_chat_id',
      'message_id',
    ];
  }

  public function setChatId($id) {
    $this->chat_id = $id;
  }

  public function setFromChat_id($id) {
    if (is_numeric($id)) {
      $this->from_chat_id = $id;
    }
  }

  public function disableNotification($bool) {
    if ($bool) {
      $this->disable_notification = TRUE;
    }
  }

}