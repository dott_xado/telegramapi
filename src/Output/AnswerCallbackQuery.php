<?php

namespace dott_xado\TelegramApi\Output;

class AnswerCallbackQuery extends Method {

  protected function getRequired() {
    return [
      'callback_query_id',
    ];
  }

  public function setCallbackId($id) {
    $this->callback_query_id = $id;
  }

}