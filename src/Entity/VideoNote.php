<?php

namespace dott_xado\TelegramApi\Entity;

class VideoNote extends Entity {

  protected function getSubEntities() {
    return [
      'thumb' => 'PhotoSize',
    ];
  }

  protected function getRequired() {
    return [
      'file_id',
      'length',
      'duration',
    ];
  }

}