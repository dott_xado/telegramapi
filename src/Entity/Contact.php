<?php

namespace dott_xado\TelegramApi\Entity;

class Contact extends Entity {

  protected function getRequired() {
    return [
      'phone_number',
      'first_name',
    ];
  }

}