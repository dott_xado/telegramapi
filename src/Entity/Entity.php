<?php

namespace dott_xado\TelegramApi\Entity;

abstract class Entity {

  public function __construct($data) {
    $subEntities = $this->getSubEntities();
    foreach ($data as $parameter => $value) {
      if (array_key_exists($parameter, $subEntities)) {
        $this->$parameter = $this->createEntities($parameter, $value, $subEntities[$parameter]);
      } else {
        $this->$parameter = $value;
      }
    }
    $this->validate();
  }

  protected function createEntities($parameter, $value, $entity) {
    if (is_array($value) && !$this->isAssociativeArray($value)) {
      $array = [];
      foreach ($value as $subkey => $subvalue) {
        $array[] = $this->createEntities($parameter, $subvalue, $entity);
      }
      return $array;
    } else {
      if (!is_array($entity)) {
        $className = 'dott_xado\TelegramApi\Entity\\' . $entity;
        return new $className($value);
      } else if (is_array($entity) && is_object($value)){
        return $value;
      } else {
        throw new \Exception('(Non posso generare un oggetto da una scelta di classi) non so quale classe creare per ' . $parameter);
      }
    }
  }

  protected function getSubEntities() {
    return [];
  }

  protected function getRequired() {
    return [];
  }

  protected function getRequiredOptional() {
    return [];
  }

  public function validate() {
    $required = $this->getRequired();
    $entities = $this->getSubEntities();
    $requiredOptional = $this->getRequiredOptional();
    if (empty($required) && empty($entities)) {
      return true;
    }
    foreach ($required as $item) {
      // if there is not or is not an istance of the needed class
      if (!isset($this->$item) || isset($this->$item) && empty($this->$item) && $this->$item !== 0 || (isset($this->$item) && isset($entities[$item]) && $this->getEntityInArray($this->$item) !== 'dott_xado\TelegramApi\Entity\\' . $entities[$item])) {
        throw new \Exception('(Non è stata generata un\'entità richiesta, oppure le classi generate non sono valide) A ' . get_class($this) . ' manca ' . $item);
      }
    }
    foreach ($entities as $param => $entity) {
      if (isset($this->$param) && is_array($this->$param)) {
        foreach ($this->$param as $key => $value) {
          if ($this->getEntityInArray($value) !== 'dott_xado\TelegramApi\Entity\\' . $entity) {
            throw new \Exception('(Le classi generate non sono valide) A ' . get_class($this) . ' manca ' . $param);
          }
        }
      } else if (isset($this->$param) && ((!is_array($entity) && get_class($this->$param) !== 'dott_xado\TelegramApi\Entity\\' . $entity) || (is_array($entity) && !$this->inArrayNamespace(get_class($this->$param), $entity)))) {
        throw new \Exception('(Le classi generate non sono valide) A ' . get_class($this) . ' manca ' . $param);
      }
    }
    foreach ($requiredOptional as $required => $optionals) {
      $verify = false;
      foreach ($optionals as $optional) {
        if (!isset($this->$optional)) {
          $verify = true;
          break;
        }
      }
      if ($verify && !isset($this->$required)) {
        throw new \Exception('(Entità richiesta in mancanza di altre) A ' . get_class($this) . ' manca ' . $required);
      }
    }
    return true;
  }

  protected function getEntityInArray($element) {
    if (is_array($element)) {
      return $this->getEntityInArray(array_pop($element));
    } else {
      return get_class($element);
    }
  }

  protected function inArrayNamespace($needle, $haystack) {
    foreach ($haystack as $value) {
      if ($needle === 'dott_xado\TelegramApi\Entity\\' . $value) {
        return true;
      }
    }
    return false;
  }

  protected function getEntityInEntity($element) {
    if (is_object($element)) {
      $element = (array) $element;
    }
    return $this->getEntityInArray($element);
  }

  protected function isAssociativeArray($a) {
    foreach ($a as $key => $v) {
      if (!is_numeric($key)) return true;
    }
    return false;
  }

  protected function isMultidimensionalArray($a) {
    foreach ($a as $key => $v) {
      if (is_array($v)) return true;
    }
    return false;
  }

  public function getProperty($property, $default = null) {
    if (isset($this->$property)) {
      return $this->$property;
    }

    return $default;
  }

  protected function snakeToCamelCase($string, $capitalizeFirstCharacter = false) {

    $str = str_replace('_', '', ucwords($string, '_'));

    if (!$capitalizeFirstCharacter) {
      $str = lcfirst($str);
    }

    return $str;
  }
}