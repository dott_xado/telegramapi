<?php


namespace dott_xado\TelegramApi\Entity;


class Audio extends Entity {

  protected function getRequired() {
    return [
      'file_id',
      'duration',
    ];
  }

}