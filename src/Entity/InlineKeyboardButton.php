<?php

namespace dott_xado\TelegramApi\Entity;

class InlineKeyboardButton extends Entity {

  protected function getSubEntities() {
    return [
      'callback_game' => 'CallbackGame',
    ];
  }

  protected function getRequired() {
    return [
      'text',
    ];
  }

}