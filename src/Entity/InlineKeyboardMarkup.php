<?php

namespace dott_xado\TelegramApi\Entity;

class InlineKeyboardMarkup extends Entity {

  protected function getSubEntities() {
    return [
      'inline_keyboard' => 'InlineKeyboardButton',
    ];
  }

  protected function getRequired() {
    return [
      'inline_keyboard',
    ];
  }

  public static function getKeyboardName() {
    return 'inline_keyboard';
  }

}