<?php

namespace dott_xado\TelegramApi\Entity;

class Chat extends Entity {

  protected function getRequired() {
    return [
      'id',
      'type',
    ];
  }

}