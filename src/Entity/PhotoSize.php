<?php

namespace dott_xado\TelegramApi\Entity;

class PhotoSize extends Entity {

  protected function getRequired() {
    return [
      'file_id',
      'width',
      'height',
    ];
  }

}