<?php

namespace dott_xado\TelegramApi\Entity;

class ShippingQuery extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'shipping_address' => 'ShippingAddress',
    ];
  }

  public function handle() {

  }

  public function getChatId() {
    return $this->from->id;
  }

  public function getChat() {
    return null;
  }

}