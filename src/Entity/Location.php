<?php

namespace dott_xado\TelegramApi\Entity;

class Location extends Entity {

  protected function getRequired() {
    return [
      'longitude',
      'latitude',
    ];
  }

}