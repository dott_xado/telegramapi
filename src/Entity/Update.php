<?php

namespace dott_xado\TelegramApi\Entity;

class Update extends Entity {

  protected function getSubEntities() {
    return [
      'message' => 'Message',
      'edited_message' => 'Message',
      'channel_post' => 'Message',
      'edited_channel_post' => 'Message',
      'inline_query' => 'InlineQuery',
      'chosen_inline_result' => 'ChosenInlineResult',
      'callback_query' => 'CallbackQuery',
      'shipping_query' => 'ShippingQuery',
      'pre_checkout_query' => 'PreCheckoutQuery',
    ];
  }

  protected function getRequired() {
    return [
      'update_id',
    ];
  }

  public function getType() {
    $types = [
      'message',
      'edited_message',
      'channel_post',
      'edited_channel_post',
      'inline_query',
      'chosen_inline_result',
      'callback_query',
      'shipping_query',
      'pre_checkout_query',
    ];
    foreach ($types as $type) {
      if ($this->getProperty($type)) {
        return $type;
      }
    }

    return null;
  }

}