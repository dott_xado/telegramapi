<?php

namespace dott_xado\TelegramApi\Entity;

class ReplyKeyboardRemove extends Entity {

  protected function getRequired() {
    return [
      'remove_keyboard',
    ];
  }

  public static function getKeyboardName() {
    return 'remove_keyboard';
  }
}