<?php

namespace dott_xado\TelegramApi\Entity;

class InlineQuery extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'Location' => 'Location',
    ];
  }

  public function handle() {

  }

  public function getChatId() {
    return $this->from->id;
  }

  public function getChat() {
    return null;
  }
}