<?php

namespace dott_xado\TelegramApi\Entity;

class Venue extends Entity {

  protected function getSubEntities() {
    return [
      'location' => 'Location',
    ];
  }

  protected function getRequired() {
    return [
      'location',
      'title',
      'address',
    ];
  }

}