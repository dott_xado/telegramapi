<?php

namespace dott_xado\TelegramApi\Entity;

class MessageEntity extends Entity {

  protected function getSubEntities() {
    return [
      'user' => 'User',
    ];
  }

  protected function getRequired() {
    return [
      'type',
      'offset',
      'length',
    ];
  }



}