<?php

namespace dott_xado\TelegramApi\Entity;

class ChatPhoto extends Entity {

  protected function getSubEntities() {
    return [
      'user' => 'User',
    ];
  }

  protected function getRequired() {
    return [
      'small_file_id',
      'big_file_id',
    ];
  }

}