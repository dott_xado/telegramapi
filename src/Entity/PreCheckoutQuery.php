<?php

namespace dott_xado\TelegramApi\Entity;

class PreCheckoutQuery extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'order_info' => 'OrderInfo',
    ];
  }

  public function handle() {

  }

  public function getChatId() {
    return $this->id;
  }

  public function getChat() {
    return null;
  }

}