<?php

namespace dott_xado\TelegramApi\Entity;

class SuccessfulPayment extends Entity {

  protected function getSubEntities() {
    return [
      'order_info' => 'OrderInfo',
    ];
  }
}