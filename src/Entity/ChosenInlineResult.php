<?php

namespace dott_xado\TelegramApi\Entity;

class ChosenInlineResult extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'location' => 'Location',
    ];
  }

  public function handle() {

  }

  public function getChatId() {
    return $this->from->id;
  }

  public function getChat() {
    return null;
  }

}