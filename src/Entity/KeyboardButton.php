<?php

namespace dott_xado\TelegramApi\Entity;

class KeyboardButton extends Entity {

  protected function getRequired() {
    return [
      'text',
    ];
  }

}