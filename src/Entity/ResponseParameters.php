<?php

namespace dott_xado\TelegramApi\Entity;

class ResponseParameters extends Entity {

  protected function getSubEntities() {
    return [
      'user' => 'User',
    ];
  }

}