<?php

namespace dott_xado\TelegramApi\Entity;

class Message extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'chat' => 'Chat',
      'forward_from' => 'User',
      'forward_from_chat' => 'Chat',
      'reply_to_message' => 'Message',
      'entities' => 'MessageEntity',
      'audio' => 'Audio',
      'document' => 'Document',
      'game' => 'Game',
      'photo' => 'PhotoSize',
      'sticker' => 'Sticker',
      'video' => 'Video',
      'voice' => 'Voice',
      'video_note' => 'VideoNote',
      'new_chat_members' => 'User',
      'contact' => 'Contact',
      'location' => 'Location',
      'venue' => 'Venue',
      'new_chat_member' => 'User',
      'left_chat_member' => 'User',
      'new_chat_photo' => 'PhotoSize',
      'pinned_message' => 'Message',
      'invoice' => 'Invoice',
      'successful_payment' => 'SuccessfulPayment',
    ];
  }

  protected function getRequired() {
    return [
      'message_id',
      'date',
      'chat',
    ];
  }

  public function getType() {
    $types = array();
    if (isset($this->entities)) {
      foreach ($this->entities as $entity) {
        $types[] = $entity->type;
      }
    }
    return $types;
  }

  public function getChatId() {
    return $this->chat->id;
  }

  public function handle() {
    // gli update type sono di 4 tipi in questa classe, non so se è necessario gestirli
    $response = null;
    $types = $this->getType();
    if (in_array('bot_command', $types)) {
      $response = $this->executeCommand();
    } else {
      $response = (new \dott_xado\Bot\Message($this))->handle();
    }
    return $response;
  }

  protected function executeCommand() {
    $commandData = $this->getCommandAndPayload();
    $command_class = 'dott_xado\Bot\Command\\' . $this->snakeToCamelCase($commandData['command'], true);

    $verifyAbstract = new \ReflectionClass($command_class);
    if (class_exists($command_class) && !$verifyAbstract->isAbstract()) {
      return (new $command_class($this))->executeCommand($commandData['payload']);
    }
    return null;
  }

  protected function getCommandAndPayload() {
    $result['command'] = null;
    $result['payload'] = null;
    $text = trim($this->text);
    $username = strtolower('@' . BOT_NAME);
    $username_len = strlen($username);
    if (strtolower(substr($text, 0, $username_len)) == $username) {
      $text = trim(substr($text, $username_len));
    }

    if (preg_match('/^(?:\/([a-z0-9_]+)(@[a-z0-9_]+)?(?:\s+(.*))?)$/is', $text, $matches)) {
      $command_owner = (isset($matches[2])) ? strtolower($matches[2]) : null;

      if (!$command_owner || $command_owner == $username) {
        $result['command'] = $matches[1];

        $result['payload'] = (isset($matches[3])) ? $matches[3] : null;
      }
    }

    return $result;
  }

  public function getChat() {
    return $this->chat;
  }
}