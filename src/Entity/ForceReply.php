<?php

namespace dott_xado\TelegramApi\Entity;

class ForceReply extends Entity {

  protected function getRequired() {
    return [
      'force_reply',
    ];
  }

  public static function getKeyboardName() {
    return 'force_reply';
  }

}