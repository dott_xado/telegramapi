<?php

namespace dott_xado\TelegramApi\Entity;

class Voice extends Entity {

  protected function getRequired() {
    return [
      'file_id',
      'duration',
    ];
  }

}