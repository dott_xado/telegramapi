<?php

namespace dott_xado\TelegramApi\Entity;

class UserProfilePhotos extends Entity {

  protected function getSubEntities() {
    return [
      'photos' => 'PhotoSize',
    ];
  }

  protected function getRequired() {
    return [
      'total_count',
      'photos',
    ];
  }

}