<?php

namespace dott_xado\TelegramApi\Entity;

class CallbackQuery extends Entity {

  protected function getSubEntities() {
    return [
      'from' => 'User',
      'message' => 'Message',
    ];
  }

  protected function getRequired() {
    return [
      'id',
      'from',
      'chat_instance',
    ];
  }

  public function handle() {
    $response = $this->executeCallback();
    return $response;
  }

  protected function executeCallback() {
    $callbackData = $this->getCallbackAndPayload();
    $callback_class = 'dott_xado\Bot\Callback\\' . $callbackData['callback'];

    $verifyAbstract = new \ReflectionClass($callback_class);
    if (class_exists($callback_class) && !$verifyAbstract->isAbstract()) {
      return (new $callback_class($this))->executeCallback($callbackData['payload']);
    }
    return null;
  }

  protected function getCallbackAndPayload() {
    $result['callback'] = null;
    $result['payload'] = null;
    $text = trim($this->data);
    $explode = explode('=', $text);
    if (count($explode) == 2) {
      $result['callback'] = ucfirst($explode[0]);
      $result['payload'] = $explode[1];
    }
    return $result;
  }

  public function getChatId() {
    return $this->message->chat->id;
  }

  public function getChat() {
    return $this->message->chat;
  }

}