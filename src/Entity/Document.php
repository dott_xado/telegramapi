<?php

namespace dott_xado\TelegramApi\Entity;

class Document extends Entity {

  protected function getSubEntities() {
    return [
      'thumb' => 'PhotoSize',
    ];
  }

  protected function getRequired() {
    return [
      'file_id',
    ];
  }

}