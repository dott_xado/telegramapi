<?php

namespace dott_xado\TelegramApi\Entity;

class Game extends Entity {

  protected function getSubEntities() {
    return [
      'photo' => 'PhotoSize',
      'text_entities' => 'MessageEntity',
      'animation' => 'Animation',
    ];
  }
}