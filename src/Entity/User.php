<?php

namespace dott_xado\TelegramApi\Entity;

class User extends Entity {

  protected function getRequired() {
    return [
      'id',
      'first_name',
    ];
  }

  public function getLanguage() {
    if (isset($this->language_code)) {
      $lang_explode = explode('-', $this->language_code);
      $language = strtolower($lang_explode[0]);
      return $language;
    }


  }

}
