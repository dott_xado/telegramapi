<?php

namespace dott_xado\TelegramApi\Entity;

class ChatMember extends Entity {

  protected function getSubEntities() {
    return [
      'user' => 'User',
    ];
  }

  protected function getRequired() {
    return [
      'user',
      'status',
    ];
  }

}