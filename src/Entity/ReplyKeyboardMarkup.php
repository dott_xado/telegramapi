<?php

namespace dott_xado\TelegramApi\Entity;

class ReplyKeyboardMarkup extends Entity {

  protected function getSubEntities() {
    return [
      'keyboard' => 'KeyboardButton',
    ];
  }

  protected function getRequired() {
    return [
      'keyboard',
    ];
  }

  public static function getKeyboardName() {
    return 'keyboard';
  }

}