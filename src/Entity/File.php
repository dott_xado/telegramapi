<?php

namespace dott_xado\TelegramApi\Entity;

class File extends Entity {

  protected function getRequired() {
    return [
      'file_id',
    ];
  }

}